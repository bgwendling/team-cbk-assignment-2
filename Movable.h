#pragma once

#include <vector>
#include <iostream>
#include "ScreenObject.h"
#include "ModelHandler.h"

class Movable : public ScreenObject
{
public:
	enum direction
	{
		UP,
		LEFT,
		DOWN,
		RIGHT
	};

	Movable() {};
	Movable(Models* model, glm::vec3 position, glm::vec3 scale, glm::vec2 size) : ScreenObject(model, position, scale, size) {};
	~Movable() {};

	void move(float deltaTime);
	bool checkCollisionWith(SDL_Rect hitbox, SDL_Rect* intersection);
	bool checkCollisions(std::vector<ScreenObject>* walls, std::vector<std::vector<int>>cMap, float deltaTime);

	void chooseSetDirection(Movable::direction direction, glm::vec3 up, bool state);

	void setSpeed(float speed);

	glm::vec3 getDirection();

private:
	//--------------------------------PRIVATE VARIABLES----------------------------------
	glm::vec3 movementDirection = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 currentMovementDirection = glm::vec3(0.0f, 0.0f, 0.0f);
	float speed = 10.0f;
	
	//--------------------------------PRIVATE FUNCTIONS----------------------------------
	void setDirection(Movable::direction direction);
	void firstPersonSetDirection(Movable::direction direction, glm::vec3 up);

	void moveCollisionRect(float deltaTime, glm::vec3 direction);

};