#pragma once
#if defined(__linux__)						// If we are using linux.
#include <glm/glm.hpp>
#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <glm\glm.hpp>
#endif

#include "ScreenObject.h"

class Pickup : public ScreenObject
{
public:
	Pickup();
	Pickup(Models* model, glm::vec3 position, glm::vec3 scale, glm::vec2 size) : ScreenObject(model, position, scale, size) {};
	~Pickup();

	bool update(const glm::vec3& playerPos, const float& deltaTime);

private:
};