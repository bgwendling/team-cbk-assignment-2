#pragma warning( push )
#pragma warning( disable : 4996)

#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
#include <glm/gtx/rotate_vector.hpp>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

#include <glm\gtx\rotate_vector.hpp>
//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL2\SDL.h>
#include <gl\glew.h>
#include <SDL2\SDL_opengl.h>
#include <gl\glu.h>
#endif

#include <stdio.h>
#include <SDL2/SDL.h>
#include <string>

#include <iostream>

#include "globals.h"

#include "InputHandler.h"
#include "WindowHandler.h"
#include "ShaderHandler.h"
#include "ModelHandler.h"
#include "TextureHandler.h"
#include "LightHandler.h"

#include "Level.h"
#include "GameEvent.h"
#include "ScreenObject.h"
#include "Movable.h"
#include "Camera.h"
#include "Framebuffer.h"

//SDL defines main, we don't like that
#ifdef main
#undef main
#endif //  main

//Creates a Level object for each line in gLEVELS_FILE andplace it it the gLevels vector.
//Those lines are paths of files with map data.
//See Level::loadMap for more information.
//Returns a pointer to the first Level object (currentLevel).
Level* loadLevels(ModelHandler* modelHandler)
{
	FILE* file = fopen(gLEVELS_FILE, "r");

	int nrOfLevels;
	std::string tempPath;

	fscanf(file, "Number of files: %d", &nrOfLevels);

	for (int i = 1; i <= nrOfLevels; i++)
	{
		char tempCPath[51];
		fscanf(file, "%50s", &tempCPath);
		tempPath = tempCPath;
		Level level(tempPath, modelHandler);
		gLevels.push_back(level);
	}

	fclose(file);
	file = nullptr;

	return &gLevels.front();
}

//Initializes openGL
bool initGL()
{
	//Success flag
	bool success = true;

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

	return success;
}

/**
 * Initializes the InputHandler, WindowHandler and OpenGL
 */
bool init(bool& running)
{
	InputHandler::getInstance().init();

	if (!WindowHandler::getInstance().init())
	{
		running = false;
		return false;
	}

	initGL();

	return true;
}

//While there are events in the eventQueue. Process those events.
void update(float deltaTime, std::queue<GameEvent>& eventQueue, Camera& camera, Level* currentLevel, Movable* player, LightHandler* lightHandler)
{
	GameEvent nextEvent;
	while (!eventQueue.empty())
	{
		nextEvent = eventQueue.front();
		eventQueue.pop();

		if (nextEvent.action == ActionEnum::PLAYER_MOVE_UP)
		{
			player->chooseSetDirection(Movable::UP, camera.getUp(), camera.getFirstPerson());
		}
		else if (nextEvent.action == ActionEnum::PLAYER_MOVE_DOWN)
		{
			player->chooseSetDirection(Movable::DOWN, camera.getUp(), camera.getFirstPerson());
		}
		else if (nextEvent.action == ActionEnum::PLAYER_MOVE_LEFT)
		{
			player->chooseSetDirection(Movable::LEFT, camera.getUp(), camera.getFirstPerson());
		}
		else if (nextEvent.action == ActionEnum::PLAYER_MOVE_RIGHT)
		{
			player->chooseSetDirection(Movable::RIGHT, camera.getUp(), camera.getFirstPerson());
		}
		else if (nextEvent.action == ActionEnum::BUTTON_E)
		{
			lightHandler->switchLight();
		}
		else if (nextEvent.action == ActionEnum::SPACE)
		{
			camera.toggleFirstPerson();
		}
	}

	//checks for collision before movement
	if (!(player->checkCollisions(currentLevel->getWalls(), currentLevel->getMap(), deltaTime)))
	{
		player->move(deltaTime);
	}

	//Checks if the player has picked up any dots
	for (int i = 0; i < currentLevel->getPickups()->size(); ++i)
	{
		if (currentLevel->getPickups()->at(i).update(player->getPos(), deltaTime))
		{
			currentLevel->getPickups()->erase(currentLevel->getPickups()->begin() + i);
		}
	}

	camera.lookAt(player->getPos(), player->getDirection());
	lightHandler->update(player->getPos(), player->getDirection());
}

ShaderHandler::ShaderProgram* shaderUpdate(ShaderHandler* shaderHandler, LightHandler* lighthandler)
{
	ShaderHandler::ShaderProgram* tempShaderProgram;
	if (lighthandler->getLightSwitch())
	{
		tempShaderProgram = shaderHandler->getShader("pointLight");
	}
	else
	{
		tempShaderProgram = shaderHandler->getShader("flashLight");
	}
	return tempShaderProgram;
}

//All draw calls should originate here
void draw(Level* currentLevel, Camera* camera, ShaderHandler* shaderHandler, Movable* player, LightHandler* lightHandler, Framebuffer& depthbuffer)
{
	//Furthest point in the shadow map
	const GLfloat farPlane = 25.0f;

	ShaderHandler::ShaderProgram* shaderProgram;
	std::vector<glm::mat4> shadowTransforms;

	shaderProgram = shaderHandler->getShader("shadowShader");

	glUseProgram(shaderProgram->programId);

	//Buffer used for shadowmap
	glBindFramebuffer(GL_FRAMEBUFFER, depthbuffer.frameBufferObject);

	glClear(GL_DEPTH_BUFFER_BIT);
	
	//Creates and stores matrixes used for creating the shadow map
	lightHandler->createProjectionMatrixes(depthbuffer, shadowTransforms, *shaderProgram, farPlane);

	//Drawing the shadow map
	for (auto wall : *currentLevel->getWalls())
	{
		wall.draw(camera, shaderProgram, lightHandler);
	}
	currentLevel->getFloor()->draw(camera, shaderProgram, lightHandler);

	for (auto pickup : *currentLevel->getPickups())
	{
		pickup.draw(camera, shaderProgram, lightHandler);
	}
	
	//Drawing the scene
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	shaderProgram = shaderUpdate(shaderHandler, lightHandler);
	glUseProgram(shaderProgram->programId);

	//Texture0 is always used for shadow map
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, depthbuffer.depthBuffer);
	glUniform1i(shaderProgram->depthMapID, 0);

	for (auto wall : *currentLevel->getWalls())
	{
		wall.draw(camera, shaderProgram, lightHandler);
	}

	for (auto pickup : *currentLevel->getPickups())
	{
		pickup.draw(camera, shaderProgram, lightHandler);
	}
	currentLevel->getFloor()->draw(camera, shaderProgram, lightHandler);
	player->draw(camera, shaderProgram, lightHandler);

	//Update screen
	SDL_GL_SwapWindow(WindowHandler::getInstance().getWindow());
}

//Calls cleanup code on program exit.
void close()
{
	WindowHandler::getInstance().close();
}

int main(int argc, char *argv[])
{
	float fpsGoal = 60.0f;
	float nextFrame = 1 / fpsGoal;	//Time between frames in seconds
	float nextFrameTimer = 0.0f;	//Time from last frame in seconds
	float deltaTime = 0.0f;			//Time since last pass through of the game loop.
	auto clockStart = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop = clockStart;

	bool running = true;

	std::vector<Level> levels;

	ShaderHandler::ShaderProgram* currentShaderProgram = nullptr;

	init(running);

	std::queue<GameEvent> eventQueue; //Main event queue for the program.

	//Sets up the camera and matrixes #magicNumbers
	Camera mainCamera(glm::vec3(27.0f, 50.0f, 60.0f));
	mainCamera.setProjection(0.1f, 100.0f);
	mainCamera.setFront(glm::vec3(0.0f, -7.0f, -3.0f));
	mainCamera.lookAt(glm::vec3(25.0f, 50.0f, 60.0f));
	
	TextureHandler textureHandler;

	//Loads the models
	ModelHandler modelHandler;
	modelHandler.loadModelData("sphere", "./resources/models/player.obj", &textureHandler);
	modelHandler.loadModelData("floor", "./resources/models/floor.obj", &textureHandler);
	modelHandler.loadModelData("cube", "./resources/models/DeadTree.obj", &textureHandler);
	modelHandler.loadModelData("pickup", "./resources/models/pickup.obj", &textureHandler);

	ShaderHandler shaderHandler;
	currentShaderProgram = shaderHandler.initializeShaders();

	Level* currentLevel = loadLevels(&modelHandler);

	Movable player(modelHandler.getModel("sphere"), currentLevel->getPlayerPosition(), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec2(2.0f, 2.0f));

	LightHandler lightHandler(currentLevel->getPlayerPosition());

	Framebuffer depthBuffer((int)WindowHandler::getInstance().getScreenSize().x, (int)WindowHandler::getInstance().getScreenSize().y, SHADOWMAP_CUBE);

	while (running)
	{
		clockStart = std::chrono::high_resolution_clock::now();
		InputHandler::getInstance().readInput(eventQueue, running);
		update(deltaTime, eventQueue, mainCamera, currentLevel, &player, &lightHandler);

		if (nextFrameTimer >= nextFrame)
		{
			draw(currentLevel, &mainCamera, &shaderHandler, &player, &lightHandler, depthBuffer);
			nextFrameTimer = 0.0f;
		}

		clockStop = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();

		nextFrameTimer += deltaTime;
	}

	close();
	return 0;
}

#pragma warning(pop)