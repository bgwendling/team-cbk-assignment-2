#version 330 core

in vec4 FragPos;

uniform vec3 lightPos;
uniform float farPlane;

void main()
{
	//Distance between current fragment and light
	float lightDistance = length(FragPos.xyz - lightPos);
	
	//Normalizes lightDistance to 0-1 range
	lightDistance = lightDistance / farPlane;
	
	gl_FragDepth = lightDistance;
}