#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 textcoords;

//Sends variables to the fragment shader
out VS_OUT 
{
	out vec3 Normal;
	out vec3 FragPos;
	out vec2 TextCoords;
} vs_out;
	
// Values that stay constant for the whole mesh.
uniform mat4 MVP;
uniform mat4 modelMatrix;

void main()
{
	//Set position of vertex
	gl_Position =  MVP * vec4(vertexPosition_modelspace, 1.0);
	
	//Sends info to fragment shader
	vs_out.FragPos = vec3(modelMatrix * vec4(vertexPosition_modelspace, 1.0));
	vs_out.Normal = transpose(inverse(mat3(modelMatrix))) * normal.xyz; 
	vs_out.TextCoords = textcoords.xy;
}

