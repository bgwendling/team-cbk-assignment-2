#pragma once

#if defined(__linux__)
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)
#include <SDL2\SDL_image.h>
#include <SDL2\SDL_opengl.h>
#endif

#include <string>

/**
* Used to handle the loading of textures for use with OpenGL.
*/
class TextureHandler
{
public:
	/**
	* Loads an image and greates an OpenGL texture buffer.
	* @param imageName The path to the image we are loading.
	* @returns The texture identifier for use with OpenGL.
	*/
	GLuint createTextureFromImage(const std::string& imageName);

	/**
	* Creates a 2D texture with the specified size and filtering.
	* @param textureWidth The width of the texture.
	* @param textureHeight The height of the texture.
	* @param filter specifies the filtering method to be used, should be GL_LINEAR or GL_NEAREST.
	*/
	GLuint createTexture(const GLsizei textureWidth, const GLsizei textureHeight, const GLint filtering = GL_NEAREST);

private:
	GLenum getCorrectTexelFormat(const SDL_Surface* textureSurface);

	GLuint textureBuffer = 0;
};