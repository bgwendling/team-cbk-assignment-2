#include "Movable.h"

/**
 * [Movable::move description]
 */
void Movable::move(float deltaTime)
{
	position += (currentMovementDirection * speed * deltaTime);

	modelMatrix[3][0] = position.x;
	modelMatrix[3][2] = position.z;
}

bool Movable::checkCollisions(std::vector<ScreenObject>* walls, std::vector<std::vector<int>> cMap, float deltaTime)
{
	SDL_Rect intersection;
	const float moveLeaway = 0.2;

	//the position on the map
	glm::vec2 mapPos = glm::vec2((position.x / 2.0f), (position.z / 2.0f));
	glm::vec2 wantedMapPos = glm::vec2((mapPos.x + movementDirection.x), (mapPos.y + movementDirection.z));
	if (wantedMapPos.x - (int)wantedMapPos.x > 0.5)
	{
		wantedMapPos.x += 0.5;
	}
	if (wantedMapPos.y - (int)wantedMapPos.y > 0.5)
	{
		wantedMapPos.y += 0.5;
	}

	//checks if the requested direction is a wall, and that the requested direction is not the same as the current direction
	if (movementDirection != currentMovementDirection && (cMap[(int)(wantedMapPos.y)][(int)(wantedMapPos.x)] != 1))
	{
		//if you want to move backwards there is no need for additional checks
		if (movementDirection == -currentMovementDirection)
		{
			currentMovementDirection = movementDirection;
		}
		//Checks below make sure that the player is within "moveLeaway" of the tile on the map (checks for both x and y), the couts are for debug info.
		else if (((mapPos.x + movementDirection.x) - (int)(mapPos.x + movementDirection.x) < moveLeaway))
		{
			if (((mapPos.y + movementDirection.z) - (int)(mapPos.y + movementDirection.z) < moveLeaway))
			{
				position = glm::vec3((int)(mapPos.x*2.0f), 0.0f, (int)(mapPos.y*2.0f));
				currentMovementDirection = movementDirection;
			}
			else if (((mapPos.y + movementDirection.z + moveLeaway) - (int)(mapPos.y + movementDirection.z + moveLeaway) < moveLeaway))
			{
				position = glm::vec3((int)(mapPos.x*2.0f), 0.0f, (int)((mapPos.y + moveLeaway)*2.0f));
				currentMovementDirection = movementDirection;
			}
		}
		else if (((mapPos.x + movementDirection.x + moveLeaway) - (int)(mapPos.x + movementDirection.x + moveLeaway) < moveLeaway))
		{
			if (((mapPos.y + movementDirection.z) - (int)(mapPos.y + movementDirection.z) < moveLeaway))
			{
				position = glm::vec3((int)((mapPos.x + moveLeaway)*2.0f), 0.0f, (int)((mapPos.y)*2.0f));
				currentMovementDirection = movementDirection;
			}
			else if (((mapPos.y + movementDirection.z + moveLeaway) - (int)(mapPos.y + movementDirection.z + moveLeaway) < moveLeaway))
			{
				position = glm::vec3((int)((mapPos.x + moveLeaway)*2.0f), 0.0f, (int)((mapPos.y + moveLeaway)*2.0f));
				currentMovementDirection = movementDirection;
			}
		}
	}

	//checks if the tile the player is moving towards is a wall (so we don't need to check for colission constantly), should improve performance.
	if (cMap[(int)(mapPos.y + currentMovementDirection.z)][(int)(mapPos.x + currentMovementDirection.x)] == 1)
	{
		moveCollisionRect(deltaTime, movementDirection);

		//Loops through the collidable objects (Just walls right now).
		for (auto wall : *walls)
		{
			//If a collision is registered against any of them. Return true
			if (checkCollisionWith(wall.getCollisionRect(), &intersection))
			{
				return true;
			}
		}
	}

	return false;
}

bool Movable::checkCollisionWith(SDL_Rect hitbox, SDL_Rect* intersection)
{
	//Uses SDL collision to check for collisions
	if (SDL_IntersectRect(&collisionRect, &hitbox, intersection) == SDL_TRUE) {
		return true;
	}
	else
	{
		return false;
	}
}

void Movable::moveCollisionRect(float deltaTime, glm::vec3 direction)
{
	collisionRect.x = position.x * collisionScaler;
	collisionRect.y = position.z * collisionScaler;
}

void Movable::chooseSetDirection(Movable::direction direction, glm::vec3 up, bool state)
{
	if (state)
	{
		firstPersonSetDirection(direction, up);
	}
	else
	{
		setDirection(direction);
	}
}

void Movable::setDirection(Movable::direction direction)
{
	switch (direction)
	{
	case(UP):
		movementDirection = glm::vec3(0.0f, 0.0f, -1.0f);
		break;

	case(LEFT):
		movementDirection = glm::vec3(-1.0f, 0.0f, 0.0f);
		break;

	case(DOWN):
		movementDirection = glm::vec3(0.0f, 0.0f, 1.0f);
		break;

	case(RIGHT):
		movementDirection = glm::vec3(1.0f, 0.0f, 0.0f);
		break;

	default:
		printf("Error!: The direction is not valid.\n");
	}
}

void Movable::firstPersonSetDirection(Movable::direction direction, glm::vec3 up)
{
	switch (direction)
	{
	//Up does nothing, because we're already moving forwards (duh)
	case(UP):
		break;

	//Uses the power of math to turn left
	case(LEFT):
		movementDirection = glm::cross(currentMovementDirection, -up);
		break;

	//Uses the power of math to turn right
	case(RIGHT):
		movementDirection = glm::cross(currentMovementDirection, up);
		break;

	//Turn around (every now and then I get a little bit lonely And you're never coming round)
	case(DOWN):
		movementDirection = -currentMovementDirection;
		break;

	default:
		printf("Error!: The direction is not valid.\n");
	}
}

//---------------------------------------SET FUNCTIONS--------------------------------
void Movable::setSpeed(float speed)
{
	this->speed = speed;
}

//--------------------------------------GET FUNCTIONS---------------------------------
glm::vec3 Movable::getDirection()
{
	return currentMovementDirection;
}