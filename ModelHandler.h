#pragma	once

#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL2\SDL.h>
#include <gl\glew.h>
#include <SDL2\SDL_opengl.h>
#include <gl\glu.h>
#endif

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <map>
#include <vector>

#include "Models.h"

struct Model
{
	GLuint VAO = 0;
	GLuint VBO = 0;
	GLuint IBO = 0;
	int numberOfIndices = 0;
	GLenum drawMode = 0;
};

class ModelHandler
{
public:
	ModelHandler() {};
	~ModelHandler() {};

	void loadModelData(std::string name, std::string path, TextureHandler* textureHandler);

	Models* getModel(std::string name);

private:
	//--------------------------------PRIVATE VARIABLES----------------------------------
	std::map<std::string, Models> models;
};