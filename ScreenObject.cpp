#include "ScreenObject.h"

ScreenObject::ScreenObject(Models* model, glm::vec3 position, glm::vec3 scale, glm::vec2 collisionSize)
{
	this->model = model;
	this->position = position;

	modelMatrix = glm::translate(glm::mat4(1.0f), position) * glm::scale(glm::mat4(1.0f), scale);
	setCollisionRect(position, collisionSize);
}

void ScreenObject::draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler)
{
	model->draw(camera, shaderProgram, lightHandler, &modelMatrix);
}

void ScreenObject::addTextures(GLuint& texture)
{
	textures.push_back(texture);
}

void ScreenObject::bindTextures(GLuint& shaderProgram)
{
	//Binds all textures belonging to this screenobject
	for (int i = 0; i < textures.size(); ++i)
	{
		//Breaks if we're trying to bind more textures then we are allowed
		if (i > 32)
		{
			printf("Error! Trying to bind more then 32 textures at once. Not allowed.\n");
			break;
		}

		//Activates and bind textures at texturei
		glActiveTexture(GL_TEXTURE1 + i);
		glBindTexture(GL_TEXTURE_2D, textures.at(i));
		glUniform1i(glGetUniformLocation(shaderProgram, ("texture" + std::to_string(i + 1)).c_str()), i + 1);
	}
}

//-----------------------------------------GET---------------------------------
SDL_Rect ScreenObject::getCollisionRect()
{
	return  collisionRect;
}

glm::mat4 ScreenObject::getModelMatrix()
{
	return  modelMatrix;
}

glm::vec3 ScreenObject::getPos()
{
	return position;
}


//-----------------------------------------SET-----------------------------------
void ScreenObject::setModel(Models* model)
{
	this->model = model;
}

void ScreenObject::setModelMatrix(glm::mat4 modelMatrix)
{
	this->modelMatrix = modelMatrix;
}

void ScreenObject::setPosition(glm::vec3 position)
{
	this->position = position;
	modelMatrix = glm::translate(glm::mat4(1.0f), position);
	setCollisionRect(position, glm::vec2(collisionRect.w, collisionRect.h));
}

void ScreenObject::setCollisionRect(glm::vec3 position, glm::vec2 size)
{
	SDL_Rect newCollisionRect;
	newCollisionRect.x = position.x * collisionScaler;
	newCollisionRect.y = position.z * collisionScaler;
	newCollisionRect.w = size.x * collisionScaler;
	newCollisionRect.h = size.y * collisionScaler;

	collisionRect = newCollisionRect;
}