#pragma warning( push )
#pragma warning( disable : 4996)

#include "ModelHandler.h"

/**
 * Loads model data from file.
 */
void ModelHandler::loadModelData(std::string name, std::string path, TextureHandler* textureHandler)
{
	models.insert(std::pair<std::string, Models>(name, Models(path, textureHandler)));
}

Models* ModelHandler::getModel(std::string name)
{
	if (models.count(name))
	{
		return &models[name];
	}
	else
	{
		printf("Error!: No model by name %s could be found.\n", name.c_str());
		return nullptr;
	}
}

#pragma warning(pop)