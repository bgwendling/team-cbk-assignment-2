#pragma warning( push )
#pragma warning( disable : 4996)

#include "Level.h"
#include "WindowHandler.h"

Level::Level(std::string filepath, ModelHandler* modelHandler)
{
	this->modelHandler = modelHandler;
	loadMap(filepath);
	createMap();
}

/*
Reads a text file containing map data in the following format:
5x5
1 1 1 1 1
1 0 0 0 1
2 0 1 0 0
1 0 0 0 1
1 1 1 1 1
The first line defines the dimensions of the map
The following matrix has 1s representing walls and 0s representing open space.
2 represents Pac Mans start position.
*/
void Level::loadMap(std::string filepath)
{
	int x, y, temp;
	filepath = filepath;
	FILE* file = fopen(filepath.c_str(), "r");

	fscanf(file, "%dx%d", &x, &y);

	for (int i = 0; i < y; i++)
	{
		std::vector<int> row;
		for (int j = 0; j < x; j++)
		{
			fscanf(file, " %d", &temp);
			row.push_back(temp);
		}
		map.push_back(row);
	}

	fclose(file);
	file = nullptr;
}

//Uses the map data to create ScreenObjects that represent the walls of the map.
void Level::createMap()
{
	glm::vec2 pos, size;
	size = WindowHandler::getInstance().getScreenSize();
	//CubeSize is our chosen size for all cubes
	const float CubeSize = 2.0f;
	int rows = map.size();
	int cells = 0;
	for (int i = 0; i < rows; i++)
	{
		cells = map[i].size();

		for (int j = 0; j < cells; j++)
		{
			if (map[i][j] == WALL)
			{
				ScreenObject wall(modelHandler->getModel("cube"), glm::vec3(j * CubeSize, -1.0f, i * CubeSize), glm::vec3(5.0f, 5.0f, 5.0f), glm::vec2(CubeSize, CubeSize));
				walls.push_back(wall);
			}
			else if (map[i][j] == PACMAN)
			{
				playerPosition = glm::vec3(j*CubeSize, 0.0f, i* CubeSize);
			}
			else if (map[i][j] == PICKUP)
			{
				Pickup pickup(modelHandler->getModel("pickup"), glm::vec3(j * CubeSize, -0.5f, i * CubeSize), glm::vec3(0.4f, 0.4f, 0.4f), glm::vec2(CubeSize, CubeSize));
				pickups.push_back(pickup);
			}
		}
	}
	levelFloor = ScreenObject(modelHandler->getModel("floor"), glm::vec3(cells - 1.0, -CubeSize, rows - 1.0), glm::vec3(cells, 1.0f, rows), glm::vec2(CubeSize, CubeSize));
}

//--------------------------------------------GET FUNCTIONS------------------------------------
std::vector<ScreenObject>* Level::getWalls()
{
	return &walls;
}

ScreenObject* Level::getFloor()
{
	return &levelFloor;
}

glm::vec3 Level::getPlayerPosition()
{
	return playerPosition;
}

std::vector<Pickup>* Level::getPickups()
{
	return &pickups;
}

std::vector<std::vector<int>> Level::getMap()
{
	return map;
}

#pragma warning(pop)