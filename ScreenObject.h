#pragma	once

#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL2\SDL.h>
#include <gl\glew.h>
#include <SDL2\SDL_opengl.h>
#include <gl\glu.h>
#endif

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "LightHandler.h"
#include "Camera.h"
#include "ModelHandler.h"

class ScreenObject
{
public:
	ScreenObject() {};
	ScreenObject(Models* model, glm::vec3 position, glm::vec3 scale, glm::vec2 collisionSize);
	~ScreenObject() {};

	void draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler);

	SDL_Rect getCollisionRect();
	glm::mat4 getModelMatrix();
	glm::vec3 getPos();

	void setModel(Models* model);
	void setModelMatrix(glm::mat4 modelMatrix);
	void setPosition(glm::vec3 position);

	void addTextures(GLuint& texture);
	void bindTextures(GLuint& shaderProgram);

protected:
	//--------------------------------PROTECTED VARIABLES--------------------------------
	int collisionScaler = 10000; //scales the SDL_rects that collisions are checked with up (to get more accurate colission), higher collision scaler = more precise collision.

	SDL_Rect collisionRect;
	glm::mat4 	modelMatrix;
	glm::vec3	position;

	//--------------------------------PROTECTED FUNCTIONS--------------------------------
	void setCollisionRect(glm::vec3 position, glm::vec2 size);

private:
	//--------------------------------PRIVATE VARIABLES----------------------------------
	Models* model;
	std::vector<GLuint> textures;
};