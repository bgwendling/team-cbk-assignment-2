#include "Mesh.h"

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures)
{
	this->vertices = vertices;
	this->indices = indices;
	this->textures = textures;

	setupMesh();
}

void Mesh::draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler, glm::mat4* modelMatrix)
{
	//Used for uniform naming
	GLuint diffuseNr = 1;
	GLuint specularNr = 1;

	//Max distance in shadow map
	const GLfloat farPlane = 25.0f;

	glUseProgram(shaderProgram->programId);

	for (GLuint i = 0; i < this->textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE1 + i);

		std::stringstream ss;
		std::string number;
		std::string name = this->textures[i].type;
		if (name == "textureDiffuse")
		{
			ss << diffuseNr++;	//transfers GLuint to stringstream
		}
		if (name == "textureSpecular")
		{
			ss << specularNr++;	//transfers GLuint to stringstream
		}
		number = ss.str();

		glUniform1i(glGetUniformLocation(shaderProgram->programId, (name + number).c_str()), i + 1);
		glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
	}

	//Sends MVP matrix and modelMatrix;

	glm::mat4 MVPMatrix = camera->getProjectionMatrix() * camera->getViewMatrix() *  (*modelMatrix);

	glUniformMatrix4fv(shaderProgram->MVPId, 1, GL_FALSE, glm::value_ptr(MVPMatrix));
	glUniformMatrix4fv(shaderProgram->modelMatrixId, 1, GL_FALSE, glm::value_ptr(*modelMatrix));

	glUniform1f(shaderProgram->farPlaneID, farPlane);
	//Sends camera position and switchLight bool (Decideds which light to use)
	glUniform3f(shaderProgram->cameraPositionId, camera->getPos().x, camera->getPos().y, camera->getPos().z);
	//Both Lights use these variables
	//Position of light
	glUniform3f(shaderProgram->lightPositionId, lightHandler->getLight().position.x, lightHandler->getLight().position.y, lightHandler->getLight().position.z);
	//Diffuse color
	glUniform3f(shaderProgram->lightDiffuseId, lightHandler->getLight().diffuse.x, lightHandler->getLight().diffuse.y, lightHandler->getLight().diffuse.z);
	//Specular color
	glUniform3f(shaderProgram->lightSpecularId, lightHandler->getLight().specular.x, lightHandler->getLight().specular.y, lightHandler->getLight().specular.z);

	//SpotLight
	//Constants to make the light go dimmer at distance
	glUniform1f(shaderProgram->lightConstantId, lightHandler->getLight().Constant);
	glUniform1f(shaderProgram->lightLinearId, lightHandler->getLight().linear);
	glUniform1f(shaderProgram->lightQuadraticId, lightHandler->getLight().quadratic);
	//FlashLight
	//The way we point the light
	if (!lightHandler->getLightSwitch())
	{
		glUniform3f(shaderProgram->flashLightDirectionId, lightHandler->getLight().direction.x, lightHandler->getLight().direction.y, lightHandler->getLight().direction.z);
		//Angles for where the light shouldn't shine and grows dimmer
		glUniform1f(shaderProgram->flashLightCutoffId, lightHandler->getLight().cutoff);
		glUniform1f(shaderProgram->flashLightOuterCutoffId, lightHandler->getLight().outerCutoff);
	}

	//Drawing the mesh below
	glBindVertexArray(this->VAO);
	glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
	glUseProgram(0);
}

void Mesh::setupMesh()
{
	glGenVertexArrays(1, &this->VAO);
	glGenBuffers(1, &this->VBO);
	glGenBuffers(1, &this->EBO);

	glBindVertexArray(this->VAO);
	glBindBuffer(GL_ARRAY_BUFFER, this->VBO);

	//struct already has the data in the way glBufferData expects <3
	glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(Vertex), &this->vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(GLuint), &this->indices[0], GL_STATIC_DRAW);

	//Vertex Positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

	//Vertex Normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Normal));

	//Vertex Texture Coords
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));

	glBindVertexArray(0);
}