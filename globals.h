#pragma	once

#include <vector>

#include "ScreenObject.h"
#include "Level.h"

extern const char gWINDOW_CONFIGURATION_FILE[];
extern const char gLEVELS_FILE[];

extern bool gRunning;
extern float gFpsGoal;

extern std::vector<Level> gLevels;