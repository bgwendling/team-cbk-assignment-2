#pragma once

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Camera
{
public:
	Camera() {};
	Camera(glm::vec3 position);

	void lookAt(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 front = glm::vec3(0.0f, 0.0f, 0.0f));

	void setPosition(glm::vec3 pos);
	void setProjection(float cameraNear, float cameraFar, float fov = glm::quarter_pi<float>() * 1.5f);

	glm::vec3 getPos();
	glm::vec3 getUp();

	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix();

	bool getFirstPerson();

	void setFront(glm::vec3 front);

	void toggleFirstPerson();
	//Used for camera movement while debugging

private:
	//--------------------------------PRIVATE VARIABLES----------------------------------
	bool firstPerson;

	glm::vec3 position;

	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;

	glm::vec3 front = glm::vec3(1.0f, 0.0f, 0.0f);
	glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
	glm::vec2 oldMousePosition = glm::vec2(0.0f, 0.0f);
};