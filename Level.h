#pragma	once

#include <string>
#include <vector>
#include <glm/glm.hpp>
#include "Movable.h"
#include "ScreenObject.h"
#include "Pickup.h"

class Level
{
public:
	Level(std::string filepath, ModelHandler* modelHandler);

	std::vector<ScreenObject>* getWalls();
	ScreenObject* getFloor();
	glm::vec3 getPlayerPosition();
	std::vector<Pickup>* getPickups();

	std::vector<std::vector<int>> getMap();
private:
	enum screenObjectNumber
	{
		AIR,
		WALL,
		PACMAN,
		PICKUP
	};

	//--------------------------------PRIVATE VARIABLES----------------------------------
	std::vector<std::vector<int>> map;
	std::vector<ScreenObject> walls;
	std::vector<Pickup> pickups;
	ScreenObject levelFloor;
	glm::vec3 playerPosition;
	ModelHandler* modelHandler;

	//-------------------------------PRIVATE FUNCTIONS-----------------------------------
	void loadMap(std::string filepath); //Loads map data from file
	void createMap();	//This should create a ScreenObject for each wall segment.
};